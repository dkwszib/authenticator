package pl.edu.wszib.authenticator.configuration

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import pl.edu.wszib.authenticator.enum.UserRole


@Configuration
@EnableWebSecurity
class SecurityConfiguration(@Value("\${auth.role.admin.username}") private val usernameAdmin: String,
                            @Value("\${auth.role.admin.password}") private val passwordAdmin: String,
                            @Value("\${auth.role.user.username}") private val usernameUser: String,
                            @Value("\${auth.role.user.password}") private val passwordUser: String,
                            @Value("\${auth.role.customer.username}") private val usernameCustomer: String,
                            @Value("\${auth.role.customer.password}") private val passwordCustomer: String) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity?) {
        http?.let {
            it
                    .csrf().disable()
                    .httpBasic().and()
                    .authorizeRequests()
                    .antMatchers("/singup").authenticated()
                    .antMatchers("/admin/**").hasAuthority(UserRole.ROLE_ADMIN.value)
                    .antMatchers("/owner/**").hasAnyAuthority(UserRole.ROLE_ADMIN.value, UserRole.ROLE_USER.value)
                    .antMatchers("/customer/**").hasAnyAuthority(UserRole.ROLE_ADMIN.value, UserRole.ROLE_USER.value, UserRole.ROLE_CUSTOMER.value)
                    .anyRequest().authenticated()
        }
    }

    @Autowired
    fun configureGlobally(auth: AuthenticationManagerBuilder) {
        auth.inMemoryAuthentication()
                .withUser(usernameAdmin).password(passwordAdmin).authorities(UserRole.ROLE_ADMIN.value, UserRole.ROLE_USER.value, UserRole.ROLE_CUSTOMER.value).and()
                .withUser(usernameUser).password(passwordUser).authorities(UserRole.ROLE_USER.value, UserRole.ROLE_CUSTOMER.value).and()
                .withUser(usernameCustomer).password(passwordCustomer).authorities(UserRole.ROLE_CUSTOMER.value)
    }
}