package pl.edu.wszib.authenticator.enum


enum class UserRole(val value: String) {
    ROLE_ADMIN("ADMIN"),
    ROLE_USER("USER"),
    ROLE_CUSTOMER("CUSTOMER")
}