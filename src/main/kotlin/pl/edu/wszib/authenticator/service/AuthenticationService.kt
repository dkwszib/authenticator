package pl.edu.wszib.authenticator.service

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import mu.KLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Service
import pl.edu.wszib.authenticator.enum.UserRole
import java.time.Instant
import java.util.*
import java.util.stream.Collectors
import javax.servlet.http.HttpServletResponse


@Service
class AuthenticationService(@Value("\${jwt.secret}") private val secret: String,
                            @Value("\${jwt.expires}") private val expirationTime: Long,
                            @Value("\${jwt.issuer}") private val issuer: String) {

    companion object : KLogging()

    fun authenticate(response: HttpServletResponse) {
        logger.info { "Generating token for current user" }
        val authentication = SecurityContextHolder.getContext().authentication
        val token = generateToken(authentication)
        response.setHeader("Authorization", "Bearer $token")
        logger.info { "Token generated; returning" }
    }

    private fun generateToken(authentication: Authentication): String? {
        logger.debug { "Generating token for user ${authentication.principal}" }
        val userAuthorities = authentication.authorities
                .stream()
                .map { grantedAuthority -> grantedAuthority.authority }
                .collect(Collectors.toSet())

        val userRole = resolveUserRole(userAuthorities)

        return JWT.create()
                .withSubject((authentication.principal as User).username)
                .withIssuer(issuer)
                .withClaim("role", userRole.value)
                .withExpiresAt(Date.from(Instant.now().plusSeconds(expirationTime)))
                .sign(Algorithm.HMAC256(secret))

    }

    private fun resolveUserRole(userAuthorities: MutableSet<String>) = when {
        userAuthorities.contains(UserRole.ROLE_ADMIN.value) -> UserRole.ROLE_ADMIN
        userAuthorities.contains(UserRole.ROLE_USER.value) -> UserRole.ROLE_USER
        else -> UserRole.ROLE_CUSTOMER
    }
}