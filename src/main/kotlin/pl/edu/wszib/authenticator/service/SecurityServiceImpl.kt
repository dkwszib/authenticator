package pl.edu.wszib.authenticator.service

import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service


@Service
class SecurityServiceImpl(val authenticationManager: AuthenticationManager) : SecurityService {

    companion object {
        val log = LoggerFactory.getLogger(SecurityServiceImpl::class.java)
    }

    override fun signup(): String? {
        val userDetails = SecurityContextHolder.getContext().authentication.details
        return (userDetails as? UserDetails)?.username

    }

    override fun autologin(username: String, password: String) {
    }
}