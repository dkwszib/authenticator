package pl.edu.wszib.authenticator.service

interface SecurityService {
    fun autologin(username: String, password: String)
    fun signup(): String?
}