package pl.edu.wszib.authenticator.controller

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import pl.edu.wszib.authenticator.service.AuthenticationService
import javax.servlet.http.HttpServletResponse

@RestController
class AuthenticationController(private val authenticationService: AuthenticationService) {

    @GetMapping("/signup")
    @ResponseStatus(HttpStatus.OK)
    fun signup(response: HttpServletResponse) {
        RegistrationController.log.info("inside signup controller")
        authenticationService.authenticate(response)
    }
}