package pl.edu.wszib.authenticator.controller

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.RestController
import pl.edu.wszib.authenticator.service.SecurityService


@RestController
class RegistrationController(val securityService: SecurityService) {

    companion object {
        val log = LoggerFactory.getLogger(RegistrationController::class.java)
    }

}